import AppController from "./controllers";
import readLine from 'readline';

const app = new AppController();
let rl = readLine.createInterface(process.stdin, process.stdout);

rl.question('Enter your Input :: ', (directionData) => {
    app.getDirection(directionData);
    rl.close();
});