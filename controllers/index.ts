import AppService from "../services";
import { Output } from "../interfaces";

class AppController {
    getDirection(data: string) {
        const response: Output = AppService.getDirection(data);
        console.log(response);
        
        return response;
    }
}

export default AppController;