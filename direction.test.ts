import AppController from "./controllers";

describe('Test DirectionProcess', () => {
    let app = new AppController();

    test('case 1: "W5RW5RW2RW1R" ', () => {
        const data = 'W5RW5RW2RW1R';
        const response = app.getDirection(data);

        expect(response).toEqual({
            message: 'php maqebot.php W5RW5RW2RW1R',
            X: 4,
            Y: 3,
            Direction: 'N'
        });
    });

    test('case 2: "RRW11RLLW19RRW12LW1" ', () => {
        const data = 'RRW11RLLW19RRW12LW1';
        const response = app.getDirection(data);

        expect(response).toEqual({
            message: 'php maqebot.php RRW11RLLW19RRW12LW1',
            X: 7,
            Y: -12,
            Direction: 'S'
        });
    });

    test('case 3: "LLW100W50RW200W10" ', () => {
        const data = 'LLW100W50RW200W10';
        const response = app.getDirection(data);

        expect(response).toEqual({
            message: 'php maqebot.php LLW100W50RW200W10',
            X: -210,
            Y: -150,
            Direction: 'W'
        });
    });

    test('case 4: "LLLLLW99RRRRRW88LLLRL" ', () => {
        const data = 'LLLLLW99RRRRRW88LLLRL';
        const response = app.getDirection(data);

        expect(response).toEqual({
            message: 'php maqebot.php LLLLLW99RRRRRW88LLLRL',
            X: -99,
            Y: 88,
            Direction: 'E'
        });
    });

    test('case 5: "W55555RW555555W444444W1" ', () => {
        const data = 'W55555RW555555W444444W1';
        const response = app.getDirection(data);

        expect(response).toEqual({
            message: 'php maqebot.php W55555RW555555W444444W1',
            X: 1000000,
            Y: 55555,
            Direction: 'E'
        });
    });

});