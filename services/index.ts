import { Output } from "../interfaces";
import DirectionProcess from "../helpers";

class AppService {
    static getDirection(data: string): Output {

        let X = 0;
        let Y = 0;
        let direction = 'N';

        const directionProcess = new DirectionProcess();
        let actions = data.match(/R|L|W\d+/g);

        if (actions) {
            for (let value of actions) {
                if (value === 'R') {
                    direction = directionProcess.turnRight(direction);
                } else if (value === 'L') {
                    direction = directionProcess.turnLeft(direction);
                } else if (value.startsWith('W')) {
                    const steps = parseInt(value.substring(1), 10);
                    [X, Y] = directionProcess.walkStraight(direction, X, Y, steps);
                }
            }
        }
        
        const dataRes: Output = {
            message: `php maqebot.php ${data}`,
            X: X,
            Y: Y,
            Direction: direction
        }

        return dataRes;
    }
}

export default AppService;