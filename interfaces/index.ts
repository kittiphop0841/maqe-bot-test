export interface Output {
    message: string;
    X: number;
    Y: number;
    Direction: string;
}