class DirectionProcess {
    private directions: string[];

    constructor() {
        this.directions = ['N', 'E', 'S', 'W'];
    }

    turnLeft(currentDirection: string): string {
        const currentIndex = this.directions.indexOf(currentDirection);
        const newIndex = (currentIndex - 1 + this.directions.length) % this.directions.length;
        return this.directions[newIndex];
    }

    turnRight(currentDirection: string): string {
        const currentIndex = this.directions.indexOf(currentDirection);
        const newIndex = (currentIndex + 1) % this.directions.length;
        return this.directions[newIndex];
    }

    walkStraight(currentDirection: string, x: number, y: number, steps: number): [number, number] {
        switch (currentDirection) {
            case 'N':
                y += steps;
                break;
            case 'E':
                x += steps;
                break;
            case 'S':
                y -= steps;
                break;
            case 'W':
                x -= steps;
                break;
        }
        return [x, y];
    }
}

export default DirectionProcess;