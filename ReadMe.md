
## Overview

This project is `MAQE Bot` challenge


## Getting Start

### Prerequisites
- [Node](https://nodejs.org/)  (version 20)
- [Npm](https://www.npmjs.com/) (version 10)
- [Typescript](https://www.typescriptlang.org/) (version 5.3.2)

### Installation

```bash

 npm install

```

```bash

 npm start

```

### Unit test

```bash

npm  test

```